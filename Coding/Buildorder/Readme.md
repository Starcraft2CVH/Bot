# Buildorder
Idee dieses Modul ist es, mit Hilfe einer externen Datei dem Agenten eine
beliebige Strategie / Baureihenfolge vorzugeben die dieser umsetzt.
Zusätzlich stellt dieses Modul eine Datenbank zur Verfügung, die alle relevanten
Informationen über alle Einheiten im Spiel enthält wie zum Beispiel Baukosten.

## Funktsionsausblick
Die Datei die geladen wird enthält die Reihenfolge welche Gebäude oder Einheiten
gebaut werden sollen. Zusätzlich kann sie zum Beispiel noch Bedingungen enthalten
die erfüllt sein sollen bevor der nächste Auftrag ausgegeben wird.

Sind alle Bedingungen für den nächsten Auftrag erfüllt zum Beispiel das der vorherige
abgeschlossen ist, wird dieser an das Zuständige Modul übergeben. Sobald dieses Modul
meldet, dass der Auftrag umgesetzt wurde werden die Bedingungen für den nächsten
Auftrag überprüft.

## Aufbau der Builorder

## Aufbau der Datenbank
Die Datenbank enthält folgende Einträge in eime Dictionary:
id, name, race, mineralCharge, vespeneCharge, supplyCharge, buildingTime, buildFromId, buildFromName, hitpoints, energy, armor, shields

Die Einträge haben folgende Bezeichnungen im Dictionary:

ID = "sc2Nr"                       Unit ID des Gebäudes oder einer Einheit wie in pysc2.
NAME = "easy Unit"                 Name der Einheit im Spiel o. generell.
RACE = "race"                      Terr, Prot, Zerg.
MINERAL_CHARGE = "easy Minerals"   Höhe der Kosten Kristall.
VESPENE_CHARGE = "easy Gas"        Höhe der Kosten Vespene.
SUPPLY_CHARGE  = "easy Supply"     Wie viele Wohneinheiten belegt die Einheit. !Nicht für Gebäude!
BUILDING_TIME = "easy Build time"  Bauzeit der Einheit oder Gebäude.
