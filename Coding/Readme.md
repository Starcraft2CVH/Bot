# How To
Um den Agenten auszuführen, muss der Inhalt dieses Ordners "Bot/Coding/" in den pySC2 agents Ordner kopiert werden.
Sofern pySC2 installiert ist befindet sich der agents Ordner meist in:
    "<python_verzeichnis>/Lib/site-packages/pysc2/agents"

Sind alle Dateien in das Verzeichnis kopiert, kann der Agent über die Kommandozeile gestartet werden:
    "python -m pysc2.bin.agent --map Simple64 --agent pysc2.agents.Agent.sc2_agent.SC2Agent --agent_race T"

Mit --map wird die zu startende Map ausgewählt.
Mit --agent wird der Agent ausgewählt.
Mit --agent_race wir die Rasse ausgewählt, die der Agent spielen soll.

Benötigte Packages (Python):
    pysc2, pynput(debugging)

# Modulbeschreibung
Kurze Beschreibung der Module, welche Funktionen sie haben und welche Schnittstellen sie bereitstellen.

## Agent
### Funktionen:
Schnittstelle zwischen den Modulen  
enthält step() und macht die FunctionCalls  
### Schnittstellen:


## Abstractions
### Funktionen:
Entnimmt alle notwendigen Informationen aus den Observations und bereitet diese für die anderen Module vor.  
Erstellt eine Liste mit allen bekannten Gebäuden und Einheiten und deren Attributen.  
Bestimmt wo der Bildschirm hinbewegt wird.  
###	Schnittstellen:
setObservations(Obs)  
getListAll()  
getListByID(int id)  
getMap()  
getScreenPosition()  
getScreenMove(int map_x, int map_y)    

## Buildorder
### Funktionen:
Liest Buildorder aus Datei aus.  
Empfiehlt das als nächtes zu bauende Objekt.  
### Schnittstellen:
getNextUnitId()  
get...()

## UnitDictionary
### Funktionen: 
Stellt die Datenbank zur Verfügung.
Enthält ein Dictionary, dass Informationen zu allen Einheiten und Gebäuden bereitstellt.  
### Schnittstellen:
getDataset(int id)  
get...()  

## Construction Builder
### Funktionen:
Sucht Bauplatz  
Bau auslösen  
Bau überwachen  
###	Schnittstellen:
setNextBuilding(id)  
getStatus()  

## Unit Builder
### Funktionen:
Sammelpunkte setzen  
Gebäude für Bau auswählen  
Einheiten bau in Auftrag geben  
###	Schnittstellen:
setUnits(UnitId + Count)  
getStatus()  

## Ressources
### Funktionen:
Verwaltet die Arbeiter  
Verwaltet die Mineralienfelder  
Buchführung: Wie viele Ressourcen habe ich, wie viele brauche ich, wie viele sind verplant  
## Schnittstellen:
getWorkerCoordinates()  
getRessources()  
checkIfPossible(Resources)  
reserveMinerals(Resources)  
