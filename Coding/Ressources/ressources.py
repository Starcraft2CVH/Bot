from pysc2.lib import actions
from pysc2.lib import features
from pysc2.lib import point
from pysc2.agents.ConstructionBuilder.construction_builder import Building
import pysc2.agents.UnitDictionary.unitDictionary as unitDictionary

# Action ids to do basic operations
SELECT_POINT = actions.FUNCTIONS.select_point.id
SELECT_RECT = actions.FUNCTIONS.select_rect.id
SELECT_UNIT = actions.FUNCTIONS.select_unit.id
SELECT_IDLE_WORKER = actions.FUNCTIONS.select_idle_worker.id
MOVE_CAMERA = actions.FUNCTIONS.move_camera.id
HARVEST_RETURN_QUICK = actions.FUNCTIONS.Harvest_Return_quick.id
HARVEST_GATHER_SCREEN = actions.FUNCTIONS.Harvest_Gather_screen.id
# Features (observations)
INDEX_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
SCREEN_SELECTED = features.SCREEN_FEATURES.selected.index
# Type ID (parameters for function calls to the pysc API)
SELECT_ALL_IDLE_WORKERS_ACTION = [2]
SELECT_ONE_IDLE_WORKER_ACTION = [0]
SELECT_UNIT_ACTION_SINGLE = [0]
QUEUED_FALSE = [0]
QUEUED_TRUE = [1]
SCREEN = [0]

# Unit Ids for race depending access
ID_BASE = { 'Terr' : 18, 'Zerg' : 86, 'Prot' : 59 }
ID_VESPENE_MINE = { 'Terr' : 20, 'Zerg' : 88, 'Prot' : 61 }
ID_WORKER = { 'Terr' : 45, 'Zerg' : 104, 'Probe' : 84}
ID_MINERALFIELD = { 146, 147, 341, 483}
ID_MINERALS = 341
ID_VESPENEGEYSER = 342
# Some Screen Constants
SCREEN_END = 83
SELECT_RECTANGLE_SIZE = 5

# Index for the observations
IDX_MINERALS = 1
IDX_VESPENE = 2

class Ressources:

    STATE_READY = 0
    STATE_SELECTING = 1
    STATE_CHECKING = 2
    STATE_FILLING_UP = 4
    STATE_CHECK_RESSOURCES = 5
    STATE_SELECT_IDLE_WORKER = 6
    STATE_RETURN_IDLE_WORKER = 7

    # Initialize class specific variables
    def __init__(self, actionQueue, consBuilder, race = 'Terr', debugOutputEnabled = False):
        self.actionQueue = actionQueue
        self.consBuilder = consBuilder
        self.race = race
        self.debugOutputEnabled = debugOutputEnabled
        self.vespeneMines = []
        self.currentState = Ressources.STATE_CHECK_RESSOURCES
        self.fillUpVespeneMines = False
        self.fillUpCounter = 0
        self.newVespeneMine = None

    # Returns available resources
    def getRessources(self, obs):
        minerals = obs.observation["player"][IDX_MINERALS]
        vespene = obs.observation["player"][IDX_VESPENE]
        ressources = [minerals, vespene]
        return ressources

    # Checks if enough resources are available for a given unit
    def checkIfPossible(self, obs, unitId):
        resources = unitDictionary.getResourcesById(unitId)
        if ((self.getRessources(obs)[0] - resources[0]) >= 0) and ((self.getRessources(obs)[1] - resources[1]) >= 0):
            return True
        else:
            return False

    # Function to select a worker
    def selectWorker(self, obs, baseId = 0, safeOne = False):
        if self.hasIdleWorker(obs):
            self.selectIdleWorker()
            selected = True
        else:
            selected = self._selectMineralsCollectingWorker(obs, baseId, safeOne)
        return selected

    # Select an idle worker
    def selectIdleWorker(self):
        self.actionQueue.put(actions.FunctionCall(SELECT_IDLE_WORKER, [SELECT_ALL_IDLE_WORKERS_ACTION]))

    # Check if there are idle workers
    def hasIdleWorker(self, obs):
        if SELECT_IDLE_WORKER in obs.observation["available_actions"]:
            return (True)
        else:
            return (False)

    # Function to check whether worker is selected
    def isWorkerSelected(self, obs):
        selected = False
        if self.debugOutputEnabled:
            print('RESSOURCES: is a worker selected?')

        if obs.observation['single_select'][0][0] in ID_WORKER.values():
            selected = True

        if not selected:
            multiSelectSize = len(obs.observation['multi_select'])

            if multiSelectSize > 0:
                for unit in range(0, multiSelectSize):
                    if obs.observation['multi_select'][unit][0] in ID_WORKER.values():
                        selected = True
                        break
        return selected

    # Returns how many workers are selected
    def getCurrentSelectionCount(self, obs):
        count = 0
        if obs.observation['single_select'][0][0] in ID_WORKER.values():
            count = 1

        if count == 0:
            count = len(obs.observation['multi_select'])
        return count

    # Check worker selection
    def checkWorkerSelection(self, obs):
        if self.isWorkerSelected(obs):
            if self.fillUpVespeneMines:
                self.currentState = Ressources.STATE_FILLING_UP
            else:
                self.currentState = Ressources.STATE_CHECK_RESSOURCES
        else:
            self.currentState = Ressources.STATE_SELECTING

    # Return idle worker back to gather minerals or vespene
    def returnIdleWorker(self, obs, baseId = 0):
        if self.debugOutputEnabled:
            print('RESOURCES: Return idle worker')

        (unitX, unitY) = self._getMineralCoordinates(obs)
        # If mineral fields are already in screen get and use screen position
        if len(unitY) > 0 and len(unitX) > 0:
            self.actionQueue.put(actions.FunctionCall(HARVEST_GATHER_SCREEN, [ QUEUED_FALSE, [int(unitX[0:15].mean()), int(unitY[0:15].mean())] ]))
            return True
        # Move screen to base to see minerals
        else:
            base = self._getBase(baseId)
            self.actionQueue.put(actions.FunctionCall(MOVE_CAMERA, [ [base.getMinimapRefX(), base.getMinimapRefY()] ]))
            return False

    # Returns a worker back to work when he holds minerals or vespene
    def returnIdleWorkerQuick(self):
        if self.debugOutputEnabled:
            print('RESOURCES: Return idle worker quick')
        self.actionQueue.put(actions.FunctionCall(HARVEST_RETURN_QUICK, [QUEUED_TRUE]))

    # Sends a Worker to vespene mines
    def _sendVespeneCollectors(self, obs):
        if self.debugOutputEnabled:
                print("RESSOURCES: Send worker to gather vespene")

        if self.getCurrentSelectionCount(obs) > 1:
            self.actionQueue.put(actions.FunctionCall(SELECT_UNIT, [SELECT_UNIT_ACTION_SINGLE, [0]]))

        vespeneMine = self.vespeneMines[len(self.vespeneMines)-1]
        self.actionQueue.put(actions.FunctionCall(MOVE_CAMERA, [ [vespeneMine.getMinimapRefX(), vespeneMine.getMinimapRefY()] ]))
        self.actionQueue.put(actions.FunctionCall(HARVEST_GATHER_SCREEN, [QUEUED_FALSE, [vespeneMine.getScreenX(), vespeneMine.getScreenY()]]))
        self.fillUpCounter = self.fillUpCounter + 1

        if self.debugOutputEnabled:
            print("-------- RESSOURCES: Selection Count %d" % (self.getCurrentSelectionCount(obs)))
            print("-------- RESSOURCES: FillUp Counter %d" % (self.fillUpCounter))
            print("-------- RESSOURCES: FillUp at %d, %d" % (vespeneMine.getScreenX(), vespeneMine.getScreenY()))

        if self.fillUpCounter == 3:
            self.fillUpVespeneMines = False
            self.fillUpCounter = 0
            return True
        else:
            return False

    # Fill up vespeneMines
    def fillUpVespeneBuildings(self, obs, vespenemineListFromConstructionBuilder, baseId = 0):
        if not self.fillUpVespeneMines:
            if len(vespenemineListFromConstructionBuilder) > len(self.vespeneMines):
                for mine in vespenemineListFromConstructionBuilder:
                    if mine not in self.vespeneMines:
                        if mine.state == Building.STATE_FINISHED:
                            self.vespeneMines.append(mine)
                            self.fillUpVespeneMines = True
                            self.fillUpCounter = 0
                            # self.currentState = Ressources.STATE_SELECTING
                            return False
                            break
            return True
        else:
            finished = self._sendVespeneCollectors(obs)
            if finished:
                unitType = obs.observation["screen"][INDEX_UNIT_TYPE]
                unitY, unitX = (unitType == ID_BASE[self.race]).nonzero()
                self.actionQueue.put( actions.FunctionCall(SELECT_POINT, [SCREEN, [int(unitX.mean()), int(unitY.mean())]]) )
            return finished


    # safeOne must be true, if the worker is not for building! If false, the function selects multiple workers
    def _selectMineralsCollectingWorker(self, obs, baseId = 0, safeOne = False):
        if self.debugOutputEnabled:
            print("RESSOURCES: Select worker...")

        base = self._getBase(baseId)
        (unitX, unitY) = self._getMineralCoordinates(obs)

        # Get four points that "draw" a reactangle over the mineral field, when there is one
        if unitX != [] and unitY != []:
            xMax = max(unitX)
            xMin = min(unitX)
            yMax = max(unitY)
            yMin = min(unitY)

            # Get the middle of the mineral field
            xCenter = int( xMin + (abs(xMax - xMin) / 2.0) )
            yCenter = int( yMin + (abs(yMax - yMin) / 2.0) )

            # Dont select a position that is not on the screen (rectangle select)
            leftTop =  ( xCenter - SELECT_RECTANGLE_SIZE, yCenter - SELECT_RECTANGLE_SIZE )
            botRight = ( xCenter + SELECT_RECTANGLE_SIZE, yCenter + SELECT_RECTANGLE_SIZE )
            if leftTop[0] < 0:
                leftTop = (0, leftTop[1])
            if leftTop[1] < 0:
                leftTop = (leftTop[0], 0)
            if botRight[0] > SCREEN_END:
                botRight = (SCREEN_END, botRight[1])
            if botRight[1] > SCREEN_END:
                botRight = (botRight[0], SCREEN_END)

            self.actionQueue.put(actions.FunctionCall(SELECT_RECT, [SCREEN, [leftTop[0], leftTop[1]], [botRight[0], botRight[1]]]))

            # Choose one worker from multi selection.
            # Not neccessary when building cause only one worker starts to build but neccessary when filling up vespene mines.
            if(safeOne):
                self.actionQueue.put(actions.FunctionCall(SELECT_UNIT, [SELECT_UNIT_ACTION_SINGLE, [0]]))
            return True
        else:
            self.actionQueue.put(actions.FunctionCall(MOVE_CAMERA, [ [base.getMinimapRefX(), base.getMinimapRefY()] ]))
            return False

    # Get a base related to baseId
    def _getBase(self, baseId):
        return self.consBuilder.getListOfBuildingsById(ID_BASE[self.race])[baseId]

    # Get the position from minerals on the screen
    def _getMineralCoordinates(self, obs):
        # Get screen positions of mineral fields
        unitType = obs.observation["screen"][INDEX_UNIT_TYPE]
        unitY, unitX = (unitType == ID_MINERALS).nonzero()
        return (unitX, unitY)

    # Returns current state of the Ressource module
    def getCurrentState(self):
        return self.currentState

    # Set the current state of the Ressource module
    def setCurrentState(self, currentState):
        self.currentState = currentState
