
from pysc2.lib import actions, features

# Fix base IDs
# @TODO remove race dependency
ID_COMMANDCENTER = 18
IDX_ARMY_COUNT = 8

# Fix action IDs
ACTION_ID_MOVE_CAMERA = actions.FUNCTIONS.move_camera.id
ACTION_ID_SELECT_ARMY = actions.FUNCTIONS.select_army.id
ACTION_ID_ATTACK_MINIMAP = actions.FUNCTIONS.Attack_minimap.id
ACTION_ID_MOVE_MINIMAP = actions.FUNCTIONS.Move_minimap.id

# TEMPORARY: Fix base positions on Simple64 map
# @TODO calculate for any map.
BASE_TOP_LEFT = [21, 24]
BASE_BOTTOM_RIGHT = [39, 45]

# Configuration
ARMY_ATTACK_SIZE = 17 # At how many units the enemy should be attacked.

# Class to handle the attack process
class WarHandler():

    STATE_COLLECT_ARMY = 0
    STATE_ATTACK = 1

    # Function to initialize module
    def __init__(self, actionQueue=None, abstractionHub=None, debugOutputEnabled=False):
        self.actionQueue = actionQueue
        self.abstractionHub = abstractionHub
        self.debugOutputEnabled = debugOutputEnabled
        self.currentState = WarHandler.STATE_COLLECT_ARMY

    # Function to attack the enemy with the whole army
    def attackEnemy(self, obs):

        # Collect all army Units on the middle of the map
        if self.currentState == WarHandler.STATE_COLLECT_ARMY:
            # Select army
            if ACTION_ID_SELECT_ARMY in obs.observation["available_actions"]:
                self.actionQueue.put( actions.FunctionCall(ACTION_ID_SELECT_ARMY, [[0]]) )

            # Send units to the middle of the map
            # @TODO find the best position on every map.
            self.actionQueue.put( actions.FunctionCall(ACTION_ID_MOVE_MINIMAP, [[1], [36, 32]]) )
            if obs.observation["player"][IDX_ARMY_COUNT] > ARMY_ATTACK_SIZE:
                self.currentState = WarHandler.STATE_ATTACK

        # Attack the enemy
        if self.currentState == WarHandler.STATE_ATTACK:
            self.actionQueue.put( actions.FunctionCall(ACTION_ID_SELECT_ARMY, [[0]]) )

            attackX, attackY = self._determineAttackPosition(obs)

            self.actionQueue.put( actions.FunctionCall(ACTION_ID_ATTACK_MINIMAP, [[1], [attackX, attackY]]) )

            if obs.observation["player"][IDX_ARMY_COUNT] < (ARMY_ATTACK_SIZE - 3):
                self.currentState = WarHandler.STATE_COLLECT_ARMY

            # Move camera to show the fight (only when debug is enabled)
            if self.debugOutputEnabled:
                self.actionQueue.put(actions.FunctionCall( \
                        ACTION_ID_MOVE_CAMERA, \
                        [ [int(attackX), int(attackY)] ]) \
                        )
    # @TODO scout the enemy
    def _determineAttackPosition(self, obs):
        # Use command center position to determine position on Simple64 map
        commandCenterX = 0
        commandCenterY = 0
        units = self.abstractionHub.giveAllUnits()

        for i in range(0, len(units)):
            if units[i].type == ID_COMMANDCENTER:
                commandCenterX = int(units[i].xPos)
                commandCenterY = int(units[i].yPos)
                break;

        map = self.abstractionHub.giveTerrain()
        mapWidth = map.shape[0]
        mapHeight = map.shape[1]

        if commandCenterX < int(mapWidth/2.0) and commandCenterY < int(mapHeight/2.0):
            attackX = BASE_BOTTOM_RIGHT[0]
            attackY = BASE_BOTTOM_RIGHT[1]    
        else:
            attackX = BASE_TOP_LEFT[0]
            attackY = BASE_TOP_LEFT[1]

        return(attackX, attackY)
