#Extract Action IDs from TextFile and add them into the dictionary

fp = open('List-of-valid-Actions.txt', 'r')
lines = fp.readlines()
fp.close()

lines = [line.replace(' ', '') for line in lines]

fp = open('ActionsFreeSpaces.txt', 'w')
for line in lines:
    newLine = '/'.join(line.split('/')[1:])
    newLine = newLine.split('(', 1)[0]
    newLine = newLine + '\n'
    fp.write(newLine)
fp.close()

fp = open('ActionsFreeSpaces.txt', 'r')
lines = fp.readlines()
fp.close()

for line in lines:
    check = ['Build', 'Barracks']
    if 'Build' in line and 'Barracks' in line and not 'Reactor' in line and not 'TechLab' in line:
        print(line)
